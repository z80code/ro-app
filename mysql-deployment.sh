#!/bin/sh

echo 'Pull image MySQL'
docker pull mysql/mysql-server:5.7.21
echo 'Set tag for image'
docker tag mysql/mysql-server:5.7.21 qio01:5000/mysql-server:latest
echo 'Push image to local repo'
docker push qio01:5000/mysql-server:latest

helm install mysql